using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using Microsoft.AspNetCore.Http;

namespace ClaytonsWeb {
    public class NewsletterController : Controller {

        [HttpGet("newsletters/{filename}")]
        public IActionResult GetDocument(string filename) {      
            // string removeString = "_ocr";

            // int index = filename.IndexOf(removeString);
            // string cleanPath = (index < 0)
            //     ? filename
            //     : filename.Remove(index, removeString.Length);

            // filename = cleanPath.Replace(".txt", ".pdf");

            return PhysicalFile($"/home/kg0/ClaytonsFiles/seqsk/Newsletters/{filename}", "application/pdf");
        }
    }
}