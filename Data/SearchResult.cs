using System;

namespace ClaytonsWeb.Data
{
    public class SearchResult
    {
        public string context {get; set;}
        public string filename {get; set;}
        public int wordnum {get; set;}
        public int pagenum {get; set;}
        public int linenum {get; set;}
    }
}
